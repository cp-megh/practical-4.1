@file:Suppress("EXPERIMENTAL_IS_NOT_ENABLED")

package com.example.practical32retrofit

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.practical32retrofit.model.Users
import com.example.practical32retrofit.ui.theme.Practical32RetrofitTheme
import com.example.practical32retrofit.viewModel.MainViewModel

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Practical32RetrofitTheme {
                MaterialTheme {
                    Surface(color = MaterialTheme.colors.background) {
                        UsersList()
                    }
                }
            }
        }
    }
}

@Composable
fun UsersList() {
    val mainViewModel = hiltViewModel<MainViewModel>()
    LaunchedEffect(key1 = "Unit", block = {
        mainViewModel.getUsersList()
    })
    val data = mainViewModel.usersListResponse.collectAsState()
    LazyColumn(contentPadding = PaddingValues(15.dp)) {
        itemsIndexed(data.value) { _, item ->
            UserView(user = item, viewModel = mainViewModel)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun UserView(user: Users, viewModel: MainViewModel) {
    val context = LocalContext.current
    Card(
        modifier = Modifier
            .padding(8.dp, 4.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp,
    ) {
        Surface {
            Row(
                Modifier
                    .padding(4.dp)
                    .fillMaxWidth()
                    .combinedClickable(
                        onClick = { },
                        onLongClick = {
                            val builder = AlertDialog.Builder(context)
                            builder
                                .setMessage(
                                    "Do you want to remove " + user.name
                                            + " from list?"
                                )
                                .setCancelable(true)
                                .setPositiveButton("Yes") { _, _ ->
                                    viewModel.removeItem(user)
                                }
                                .setNegativeButton(
                                    "No"
                                ) { _, _ -> }
                            val alertDialog = builder.create()
                            alertDialog.setTitle("")
                            alertDialog.show()
                        }
                    )
            ) {
                Column(
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(start = 5.dp)
                        .align(Alignment.CenterVertically)
                )
                {
                    Image(
                        painter = painterResource(
                            id = R.drawable.ic_baseline_account_circle_24
                        ),
                        contentDescription = null
                    )
                }
                Column(
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .padding(4.dp)
                        .fillMaxWidth()
                        .weight(3f)
                ) {
                    Text(
                        text = user.name,
                        style = MaterialTheme.typography.subtitle1,
                        fontWeight = FontWeight.Bold,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    Text(
                        text = user.email,
                        style = MaterialTheme.typography.subtitle2,
                        modifier = Modifier
                            .background(
                                Color(0, 0, 0, 15)
                            )
                            .padding(4.dp),
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
                Column(modifier = Modifier.weight(2f)) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_baseline_location_on_24),
                        contentDescription = null,
                        Modifier.fillMaxWidth(),
                        alignment = Alignment.Center
                    )
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = user.address.city,
                        style = MaterialTheme.typography.body1,
                        textAlign = TextAlign.Center,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }
}