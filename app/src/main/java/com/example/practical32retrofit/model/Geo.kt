package com.example.practical32retrofit.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Geo (
    @SerializedName("lat")
    @Expose
    val lat: String,

    @SerializedName("lag")
    @Expose
    val lag: String

)