package com.example.practical32retrofit.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practical32retrofit.api.ApiService
import com.example.practical32retrofit.model.Users
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    private val _usersListResponse= MutableStateFlow<List<Users>>(emptyList())
    val usersListResponse: StateFlow<List<Users>>
    get() = _usersListResponse

    fun getUsersList(){
        viewModelScope.launch {
            val apiService= ApiService.getInstance()
            try{
                    val usersList=apiService.getUserList()
                    _usersListResponse.value= usersList
            }catch (e:Exception){

            }
        }
    }
    fun removeItem(item:Users){ _usersListResponse.value=_usersListResponse.value.filter { it!=item }.toList() }
}